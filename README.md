## MovieX

This demo project uses [TMDB](https://www.themoviedb.org/) as a remote data source and loads the current popular movies. It is written using the MVVM pattern which adapts the new [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/).

### Requirements:
* [Android Studio 3.2](https://developer.android.com/studio/preview/)

### Libraries:

* [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/)
* [Android KTX](https://github.com/android/android-ktx)
* [Kotlin Anko](https://github.com/Kotlin/anko)
* [RxJava 2](https://github.com/ReactiveX/RxJava) + [RxAndroid](https://github.com/ReactiveX/RxAndroid)
* [Dagger 2](https://github.com/google/dagger)
* [Timber](https://github.com/JakeWharton/timber)
* [Glide](https://github.com/bumptech/glide)
* [RxBus](https://github.com/joshuadeguzman/RxBus)